//
//  KSBluetoothManagerCommandListener.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 6/1/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  This is how a bluetooth device can send us commands.
//  If we wanted to handle a print command:
//
//  KSBluetoothManager.shared.addCommandListener(self, for: "pf", withDescription: "Print a string")
//
//  Then if the device sent "pf Hello World"
//
//  self's didReceiveCommand would be called with:
//  command = "pf",
//  argument = "Hello World"
//
// Usually these commands are very short because 9600 baud is slow so you are forced to
// provide a description of your command to handle both a later verbose logging and
// to help sort out command registration conflicts.
//
// The argument is anything after the space following the command, or "" if it is just the command.
//

import UIKit

@objc protocol KSBluetoothManagerCommandListener: class {
    
    // Gets called for commands we've registered for with addCommandListener for.
    func didReceiveCommand(_ command: String, argument: String)
}
