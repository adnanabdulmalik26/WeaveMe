//
//  PatternScrollViewDelegate.swift
//  WeaveMe
//
//  Created by Kurt Schaefer on 7/5/19.
//  Copyright © 2019 RetroTechJurnal. All rights reserved.
//
//  This helper class deals with splitting a pattern in a bunch of PatternLayerViews and
//  coordinating them in a scroll view.   It manages incremental rendering of the views and
//  helps coordinate the content region size, etc.
//

import UIKit

class PatternLayerViewScrollCoordinator: NSObject {

    var hasBeenSetUp: Bool {
        return !patternLayerViews.isEmpty
    }

    private let pattern: Pattern
    private let scrollView: UIScrollView
    private var patternLayerViews: [PatternLayerView] = []
    private var yetToBeRenderedViews: [PatternLayerView] = []

    init(pattern: Pattern, scrollView: UIScrollView) {
        self.pattern = pattern
        self.scrollView = scrollView
    }

    // This should be called once the scrollView's size has been established.  It will cut the pattern up into parts
    // making patternLayerViews for the entire pattern.  It puts all the views into the scroll view, updates the
    // scroll view's content size, and renders the visible parts of the pattern. If animated is true it will fade in
    // the renders once they are all done.
    func setupAndRenderVisible(animated: Bool, completion: @escaping (_ error: Error?) -> Void) {
        assert(patternLayerViews.isEmpty, "Setup should never be called more than once.")
        let generator = PatternLayerViewGenerator()

        patternLayerViews =  generator.buildPatternLayerViews(for: self.pattern, targetOutputWidth: self.scrollView.bounds.width)
        // Make them invisible so we can fade them in as a group.  If this ends up being really slow we might want some sort
        // of ordered fade in.
        for view in patternLayerViews {
            view.alpha = 0
        }
        guard let contentFrame = addViewsToScrollView(patternLayerViews) else {
            completion(nil)
            return
        }

        scrollView.contentSize = contentFrame.size
        scrollView.scrollRectToVisible(CGRect(x: 0, y: contentFrame.size.height - 1, width: contentFrame.size.width, height: 1), animated: false)

        let rect = visibleRect
        let visibleViews = patternLayerViews.filter { $0.frame.intersects(rect) }
        let offScreenViews =  patternLayerViews.filter { !$0.frame.intersects(rect) }

        let dispatchGroup = DispatchGroup()
        var renderErrors: [Error] = []

        for view in visibleViews {
            let renderer = PatternRenderer()
        
            dispatchGroup.enter()
            view.render(renderer: renderer) { (success, error) in
                if let error = error {
                    renderErrors.append(error)
                }
                dispatchGroup.leave()
            }
        }

        dispatchGroup.notify(queue: .main) {
            self.yetToBeRenderedViews = offScreenViews
            if renderErrors.first == nil {
                for view in visibleViews {
                    UIView.animate(withDuration: animated ? 0.1 : 0.0, animations: {
                        view.alpha = 1.0
                    })
                }
            }
            completion(renderErrors.first)
        }
    }

    // Right now we just do the rendering in two stages where we render the visible stuff
    // and then this.  Eventually for really long patterns we could track scrolling/update
    // incrementally.
    func renderYetToBeRenderedViews(completion: @escaping (_ error: Error?) -> Void) {
        let viewsToRender = yetToBeRenderedViews
        yetToBeRenderedViews = []

        let dispatchGroup = DispatchGroup()
        var renderErrors: [Error] = []

        for view in viewsToRender {
            let renderer = PatternRenderer()

            dispatchGroup.enter()
            view.render(renderer: renderer) { (success, error) in
                if let error = error {
                    renderErrors.append(error)
                }
                dispatchGroup.leave()
            }
        }

        dispatchGroup.notify(queue: .main) {
            UIView.animate(withDuration: 0.1, animations: {
                for view in viewsToRender {
                    view.alpha = 1.0
                }
            })
            completion(renderErrors.first)
        }
    }

    private var visibleRect: CGRect {
        return CGRect(x: scrollView.contentOffset.x, y: scrollView.contentOffset.y, width: scrollView.bounds.width, height: scrollView.bounds.height)
    }

    // These offsets are used to keep overlays from sitting right on top of one another.
    private func offsetFrame(for patternLayerView: PatternLayerView) -> CGRect {
        guard let warpCount = pattern.threadCount(.warp) else {
            return patternLayerView.frame
        }

        if warpCount == 0 {
            return patternLayerView.frame
        }
        // We introduce the same offset we use in the PatternScrollView
        // so the frame will match the offset in overlay layers.
        let scale = scrollView.bounds.width / CGFloat(warpCount)

        var yOffset: CGFloat = 0
        if let layerIndex = pattern.patternLayers.firstIndex(of: patternLayerView.patternLayer) {
            yOffset = -CGFloat(layerIndex)*scale/CGFloat(pattern.patternLayers.count)
        }

        return CGRect(x: patternLayerView.frame.minX, y: patternLayerView.frame.minY + yOffset,
                      width: patternLayerView.frame.width, height: patternLayerView.frame.height)
    }

    private func addViewsToScrollView(_ patternLayerViews: [PatternLayerView]) -> CGRect? {
        var overallFrame: CGRect?
        for view in patternLayerViews {
            view.frame = offsetFrame(for: view)
            scrollView.addSubview(view)

            if let existingFrame = overallFrame {
                overallFrame = existingFrame.union(view.frame)
            } else {
                overallFrame = view.frame
            }
        }
        return overallFrame
    }

}

extension PatternLayerViewScrollCoordinator: UIScrollViewDelegate {
    // TODO: Eventually stage in dynamic rendering/updating via scroll info so we don't just
    // render everything.
}
