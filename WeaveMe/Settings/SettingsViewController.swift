//
//  SettingsViewController.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 5/10/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, KSBluetoothManagerCommandListener {

    @IBOutlet weak var leftLimitIndicatorImageView: UIImageView!
    @IBOutlet weak var rightLimitIndicatorImageView: UIImageView!
    @IBOutlet weak var carriageLabel: UILabel!

    @IBOutlet weak var irSensorButton: UIButton!
    @IBOutlet weak var moveCarriageLeftButton: UIButton!
    @IBOutlet weak var moveCarriageRightButton: UIButton!
    
    @IBOutlet weak var jogSlider: UISlider!
    @IBOutlet weak var jogRightHeddleSlider: UISlider!

    @IBOutlet weak var warpStateView: WarpStateView!
    
    lazy var carriageJogHandler = JogHandler.init(command: "cj", commandAnnotation: "Jog Carriage", slider: jogSlider)
    lazy var rightCamJogHandler = JogHandler.init(command: "rj", commandAnnotation: "Jog Right Cam",
                                                  slider: jogRightHeddleSlider)
    lazy var leftLimitTracker = LimitIndicatorTracker.init(limitName: "Left Carriage", prefix: "L", indicatorImageView: leftLimitIndicatorImageView)
    lazy var rightLimitTracker = LimitIndicatorTracker.init(limitName: "Right Carriage", prefix: "R", indicatorImageView: rightLimitIndicatorImageView)
    
    @IBOutlet weak var sensorLineGraph: LineGraphView!
    @IBOutlet weak var lineGraphBackgroundView: UIView!
    @IBOutlet weak var sensorDismissView: UIView!
    lazy var sensorDataGraphHandler: DataGraphHandler = DataGraphHandler.init(command: "gs",
                                                                              commandAnnotation: "Get Right IR Sensor Value",
                                                                              lineGraphView: sensorLineGraph)
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var scanCamsButton: UIButton!

    @IBOutlet weak var warpLayersLabel: UILabel!
    @IBOutlet weak var warpLayersStepper: UIStepper!

    @IBOutlet weak var microstepsSegmentedControl: UISegmentedControl!
    @IBOutlet weak var sentTextDisplayLabel: UILabel!

    private var isDraggingSlider: Bool = false
    private var canSendJogValues: Bool = false
    private var unsentJogValue: Int?
    private let microstepStrings = ["1", "2", "4", "8", "16", "32"]

    // We send raw numbers to the jog loop.  128 is not jogging, 1 -> 255 for full jog range,
    // but we send 0 when we're done.
    let jogFinishedCode: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Disabled until Home and End are set.
        moveCarriageLeftButton.isEnabled = false
        moveCarriageRightButton.isEnabled = false
        scanCamsButton.isEnabled = false

        // Min and Max are abritrary in this case.
        carriageJogHandler.alphaLimitTracker = leftLimitTracker
        carriageJogHandler.betaLimitTracker = rightLimitTracker

        lineGraphBackgroundView.layer.cornerRadius = 4.0

        // With current values it seems to run between 460 and 1023
        // This just gives us some headroom.
        sensorDataGraphHandler.valueRangeMin = 400
        sensorDataGraphHandler.valueRangeMax = 1023

        KSBluetoothManager.shared.addCommandListener(listener: self, for: "pf", withDescription: "Print String")
        KSBluetoothManager.shared.addCommandListener(listener: self, for: "nc", withDescription: "New Cam Positions Available")
    }

    private func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(warpStateDidUpdate),
                                               name: Notification.Name.warpStateDidUpdate, object: nil)
    }

    private func removeObservers() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.warpStateDidUpdate, object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        KSBluetoothManager.shared.sendCommand("gm", annotation: "Get Microsteps",
                                              success: { response in
                                                if let responseString = response {
                                                    self.updateMicrosteps(responseString)
                                                }
        }, failure: { response in }, finally: {
            Loom.shared.getLoomWarpCount(completion: { (count, error) in
                guard let count = count else {
                    NSLog("Unable to get warp count \(error?.localizedDescription ?? "nil")")
                    return
                }
                if self.warpLayersStepper.value != Double(count) {
                    self.warpLayersStepper.value = Double(count)
                    self.updateWarpLayerLabel(value: count)
                }
            })
        })
    }

    private func buildTestWarpState() -> WarpState {
        let warpState = WarpState()
        for _ in 0..<60 {
            let val = Int.random(in: 0..<100)

            if val < 30 {
                warpState.layerStates.append(.liftsWithMinusLever)
            } else {
                warpState.layerStates.append(.liftsWithPlusLever)
            }
        }
        if Int.random(in: 0..<100) < 50 {
            warpState.isCurrentPhysicalState = false
        } else {
            warpState.isCurrentPhysicalState = true
        }

        return warpState
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        warpStateView.warpState = Loom.shared.currentImmediateWarpState()
        addObservers()
       //warpStateView.warpState = buildTestWarpState()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        disableSensorGraphing()
        leftLimitTracker.isActive = false
        rightLimitTracker.isActive = false
        removeObservers()
    }

    @objc func warpStateDidUpdate() {
        warpStateView.warpState = Loom.shared.currentImmediateWarpState()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sensorTap(_ sender: Any) {
        if !carriageJogHandler.isDraggingSlider && !rightCamJogHandler.isDraggingSlider {
            if sensorDataGraphHandler.pollingEnabled {
                disableSensorGraphing()
            } else {
                enableSensorGraphing()
            }
        }
    }

    private func enableSensorGraphing() {
        sensorDataGraphHandler.pollingEnabled = true
        sensorDismissView.alpha = 0.0
        sensorDismissView.isHidden = false
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
            self.lineGraphBackgroundView.layer.borderColor = UIColor.black.cgColor
            self.lineGraphBackgroundView.layer.borderWidth = 2.0
            self.sensorDismissView.alpha = 0.75
        }, completion: { finished in
        })
    }
    
    private func disableSensorGraphing() {
        sensorDataGraphHandler.pollingEnabled = false
        lineGraphBackgroundView.layer.borderColor = UIColor.clear.cgColor
        lineGraphBackgroundView.layer.borderWidth = 0.0

        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
            self.lineGraphBackgroundView.layer.borderColor = UIColor.clear.cgColor
            self.lineGraphBackgroundView.layer.borderWidth = 2.0
            self.sensorDismissView.alpha = 0.0
        }, completion: { finished in
            self.sensorDismissView.isHidden = true
        })
    }

    @IBAction func sensorDismissTap(_ sender: Any) {
        disableSensorGraphing()
    }

    @IBAction func scanCams(_ sender: Any) {
        KSBluetoothManager.shared.sendCommand("rs", annotation: "Scan All Cam Positions",
                                              success: { _ in
                                                NSLog("Scan cams was successful, but will still be in progress.")
        }, failure: { response in
                                                NSLog("Failed to scan cam positions.")
        }, finally: {})
    }

    private func updateMicrosteps(_ stepString: String) {
        guard let index = microstepStrings.firstIndex(of: stepString) else {
            NSLog("Unsupported microstep sting \(stepString)")
            return
        }
        if microstepsSegmentedControl.selectedSegmentIndex != index {
            microstepsSegmentedControl.selectedSegmentIndex = index
        }
    }

    @IBAction func microsteppingValueChanged(_ sender: Any) {
        guard let segmentedControl = sender as? UISegmentedControl else {
            return
        }
        if segmentedControl.selectedSegmentIndex < 0 || segmentedControl.selectedSegmentIndex >= microstepStrings.count {
            NSLog("Unsupported segment %d", segmentedControl.selectedSegmentIndex)
            return
        }

        let commandString = "sm " + microstepStrings[segmentedControl.selectedSegmentIndex] + " "
        KSBluetoothManager.shared.sendCommand(commandString, annotation: "Set Microsteps",
                                              success: { _ in }, failure: { _ in }, finally: {})
    }

    private func setCarriageLabel(to text: String, isDangerous: Bool = false) {
        let attributes = [NSAttributedString.Key.foregroundColor: isDangerous ? UIColor(red: 0.5, green: 0, blue: 0, alpha: 1) : UIColor.black]
        carriageLabel.attributedText = NSAttributedString(string: text, attributes: attributes)
    }

    private func setCarriageLabelToWarpIndex(isExact: Bool) {
        // Just for debugging lets ask for the current index and display it so we can verify that
        // the computed index space is as expected.
        KSBluetoothManager.shared.sendCommand("gi", annotation: "Get Carriage Warp Layer Index",
                                              success: { response in
                                                self.setCarriageLabel(to: "Carriage: \(response ?? "X")", isDangerous: !isExact)
        }, failure: { response in
            if let response = response {
                if isExact {
                    self.setCarriageLabel(to: "Carriage \(response)")
                } else {
                    self.setCarriageLabel(to: "Carriage ~\(response)", isDangerous: true)
                }
            } else {
                self.setCarriageLabel(to: "Carriage")
            }
        }, finally: {})
    }

    private func setCarriageLabelToPosition() {
        // Just for debugging lets ask for the current index and display it so we can verify that
        // the computed index space is as expected.
        KSBluetoothManager.shared.sendCommand("gp", annotation: "Get Carriage Position",
                                              success: { response in
                                                 self.setCarriageLabel(to: "Carriage: \(response ?? "X")")
        }, failure: { response in
            if let response = response {
                self.setCarriageLabel(to: "Carriage \(response)")
            } else {
                self.setCarriageLabel(to: "Carriage")
            }
        }, finally: {})
    }

    @IBAction func carriageSliderEditingDidEnd(_ sender: Any) {
        guard let slider = sender as? UISlider else {
            return
        }
        carriageJogHandler.dragEnded(slider: slider)

        // These can be useful for debugging/calibrating.
        setCarriageLabelToWarpIndex(isExact: false)
       // setCarriageLabelToPosition()
    }

    @IBAction func carriageSliderValueChanged(_ sender: Any) {
        guard let slider = sender as? UISlider else {
            return
        }
        carriageJogHandler.sliderValueChanged(slider: slider)
        disableSensorGraphing()
    }

    @IBAction func presentSensorGraphView(_ sender: Any) {
        let sensorGraphViewController = SensorGraphViewController.instantiate()
        present(sensorGraphViewController, animated: true) {
            NSLog("Sensor Graph View Controller presented.")
        }
    }

    // Group the carriage commands under c.
    @IBAction func moveCarriageRight(_ sender: Any) {
        // This moves right one warp layer.
        KSBluetoothManager.shared.sendCommand("cr", annotation: "Move Carriage Right",
                                              success: { response in
                                                self.setCarriageLabelToWarpIndex(isExact: true)
        }, failure: { response in }, finally: {})
    }

    @IBAction func moveCarriageLeft(_ sender: Any) {
        // This moves left one warp layer.
        KSBluetoothManager.shared.sendCommand("cl", annotation: "Move Carriage Left",
                                              success: { response in
                                                self.setCarriageLabelToWarpIndex(isExact: true)
        }, failure: { response in }, finally: {})
    }

    @IBAction func setCarriageHome(_ sender: Any) {
        KSBluetoothManager.shared.sendCommand("ch", annotation: "Set Carriage Home",
                                              success: { response in
                                                self.moveCarriageLeftButton.isEnabled = true
                                                self.moveCarriageRightButton.isEnabled = true
                                                self.scanCamsButton.isEnabled = true
        }, failure: { response in }, finally: {})
    }

    @IBAction func setCarriageEnd(_ sender: Any) {
       // No longer supported
    }

    private func updateWarpLayerLabel(value: Int) {
        if value == 1 {
            self.warpLayersLabel.text = String(format: "\(value) Warp Layer")
        } else {
            self.warpLayersLabel.text = String(format: "\(value) Warp Layers")
        }
    }

    @IBAction func warpLayersValueChanged(_ sender: Any) {
        guard let stepper = sender as? UIStepper else {
            return
        }
        // Don't worry about the Layer/Layers thing because you can't weave with fewer than 3 warp thread.

        // This trailing space is important! Any integer value we send needs a trailing ' '
        let command = String(format: "cn \(Int(stepper.value)) ")
        KSBluetoothManager.shared.sendCommand(command, annotation: "Set Number of Warp Layers",
                                              success: { response in
                                                self.updateWarpLayerLabel(value: Int(stepper.value))
        }, failure: { response in }, finally: {})
    }

    @IBAction func rightCamEditingDidEnd(_ sender: Any) {
        guard let slider = sender as? UISlider else {
            return
        }
        rightCamJogHandler.dragEnded(slider: slider)
    }

    @IBAction func rightHeddleCamValueChanged(_ sender: Any) {
        guard let slider = sender as? UISlider else {
            return
        }
        rightCamJogHandler.sliderValueChanged(slider: slider)
    }

    // Group the right cam motor commands around r.
    @IBAction func rotateCamCounterClockwise(_ sender: Any) {
        KSBluetoothManager.shared.sendCommand("rc", annotation: "Rotate Cam Motor Counter Clockwise",
                                              success: { response in }, failure: { response in }, finally: {})
    }

    @IBAction func rotateCamClockwise(_ sender: Any) {
        KSBluetoothManager.shared.sendCommand("rw", annotation: "Rotate Cam Motor Clockwise",
                                              success: { response in }, failure: { response in }, finally: {})
    }
    
    // TODO:  We should probably listen to connection status and deal with enables/feedback.
    // MARK: - KSBluetoothManagerCommandListener
    func didReceiveCommand(_ command: String, argument: String) {
        if command == "pf" {
            sentTextDisplayLabel.text = argument
        }
    }
}
