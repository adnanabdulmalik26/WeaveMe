//
//  CamSpaceConverter.swift
//  WeaveMe
//
//  Created by Kurt Schaefer on 5/2/19.
//  Copyright © 2019 RetroTechJurnal. All rights reserved.
//
//  This helper class clones the code on the loom's CPU that converte back and forth
//  between positions and warp indexs.   The code is kept fairy C++ like to match what's
//  going on there.
//
//  Remember that the 0th warp index is the right most warp layer on the loom.  Closest to the
//  motors.

import UIKit

class CamSpaceConverter: NSObject {

    private let numberOfWarpLayers = 60
    private let measuredPositions = [322, 3393, 6418, 9432, 12452, 15476, 18548]

    func position(from warpIndex: Int) -> Int {
        let approximateCamWidth = Float(measuredPositions[6] - measuredPositions[0])/59.0

        // We actually know that the -2 camp position should be 0 because that is were
        // the visible alignment mark is. The way we extrapolate beyond 0->59 makes -2 not be
        // guaranteed to be zero so we handle it special.
        if warpIndex == -2 {
            return 0
        }

        // Our measured positions only work in the index range 0->59 on a 60 cam loom
        // so we extrapolate beyond that.  Other than the -2 index these positions have no
        // physical measurement associated with them.
        if warpIndex + 1 < 0 {
            return measuredPositions[0] + Int(Float(warpIndex + 1)*approximateCamWidth)
        }

        if warpIndex >= 60 {
            return  measuredPositions[6] + Int(Float(warpIndex - 59)*approximateCamWidth)
        }

        // If it is in the valid range we compute it by interpolation.
        let floatIndex = Float(warpIndex + 1)/10.0
        let lowerIndex = Int(floor(floatIndex))
        let upperIndex = Int(ceil(floatIndex))

        var position = measuredPositions[lowerIndex]
        if lowerIndex != upperIndex {
            let span = Float(measuredPositions[upperIndex] - measuredPositions[lowerIndex])
            position += Int(span*(floatIndex - Float(lowerIndex)))
        }
        return position
    }

    // This is a clone of the c++ code on the board so we can more easily test it, and
    // match it's behaviour.
    func warpLayerIndex(from position: Int) -> Int {
        let approximateCamWidth = Float(measuredPositions[6] - measuredPositions[0])/59.0
        let approximateIndex = Int(floor( (Float(position) + approximateCamWidth/2.0)/approximateCamWidth) - 2)

        // This search can fail far from the origin, but for operating range positions it should work fine.
        var closestIndex = approximateIndex
        var minDistance = abs(self.position(from: approximateIndex) - position)
        var currentDistance = minDistance
        var i = approximateIndex - 2
        while i <= approximateIndex + 2 {
            currentDistance = abs(self.position(from: i) - position)
            if currentDistance < minDistance {
                closestIndex = i
                minDistance = currentDistance
            }
            i += 1
        }

        return closestIndex
    }

    // Just some tests to maker sure this is working properly.
    private func debugCamTest() {
        var i = -10
        while i < numberOfWarpLayers + 10 {
            NSLog("index \(i) = \(position(from: i)) => \(warpLayerIndex(from: position(from: i)))")
            i += 1
        }

        i = 0
        while i < 1000 {
            NSLog("position: \(i) -> \(warpLayerIndex(from: i)))")
            i += 10
        }
    }

}
